# HRMIS App

- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.6.

- [ChatGPT as Senior Frontend developer](https://chat.openai.com/c/d091694d-3a5f-43fe-8652-9d93ef6997ca).

1. Create Angular Project

```shell
ng new hrmis-app
cd hrmis-app
```

2. Install Dependencies

```shell
npm install --save grpc-web google-protobuf
ng add @angular/material
```

3. Generate gRPC Services

```shell
protoc -I=../../01-protobuf --js_out=import_style=commonjs,binary:src/app/proto --grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:src/app/proto authentication.proto
```

4. Create Login Component

```shell
ng generate component login
```

5. Implement AuthService

```shell
ng generate service auth
```

```typescript
// src/app/auth.service.ts
import { Injectable } from "@angular/core";
import { LoginRequest, LoginResponse } from "./proto/authentication_pb";
import { AuthServiceClient } from "./proto/authentication_grpc_web_pb";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private client: AuthServiceClient;

  constructor() {
    this.client = new AuthServiceClient("http://localhost:8080");
  }

  login(username: string, password: string): Promise<LoginResponse> {
    const request = new LoginRequest();
    request.setUsername(username);
    request.setPassword(password);

    return new Promise<LoginResponse>((resolve, reject) => {
      this.client.login(request, {}, (err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    });
  }
}
```

6. Set Up Routing

```shell
ng generate module app-routing --flat --module=app
```

Update app-routing.module.ts:

```typescript
const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  // Other routes (if any)
];
```

7. Implement Login Component

```typescript
// login.component.ts
import { Component } from "@angular/core";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent {
  constructor(private authService: AuthService) {}

  login(username: string, password: string): void {
    this.authService
      .login(username, password)
      .then((response) => {
        const token = response.getToken();
        console.log("Login successful. Token:", token);
      })
      .catch((error) => {
        console.error("Login error:", error);
      });
  }
}
```

8. Update Login Component HTML

```html
<!-- login.component.html -->
<mat-toolbar>
  <span>Login</span>
  <form class="login-form" #loginForm="ngForm" (ngSubmit)="login(username.value, password.value)">
    <!-- Angular Material form fields here -->
  </form>
</mat-toolbar>
```

9. Update Styles

```css
/* login.component.css */
.login-form {
  display: flex;
  align-items: center;
  margin-left: auto;
}
```

10. Update App Component HTML

```html
<!-- app.component.html -->
<mat-toolbar>
  <span>HRMIS App</span>
  <button mat-button routerLink="/login">Login</button>
</mat-toolbar>
<router-outlet></router-outlet>
```

11. Update App Module

```typescript
// app.module.ts
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, MatToolbarModule, MatInputModule, MatButtonModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
```

12. Run the App

```shell
npm run start
```
