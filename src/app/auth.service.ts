import { Injectable } from '@angular/core';
import { LoginRequest, LoginResponse } from './proto/authentication_pb';
import { AuthServiceClient } from './proto/authentication_grpc_web_pb';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private client: AuthServiceClient;

  constructor() {
    this.client = new AuthServiceClient('http://localhost:8080');
  }

  login(username: string, password: string): Promise<LoginResponse> {
    const request = new LoginRequest();
    request.setUsername(username);
    request.setPassword(password);

    return new Promise<LoginResponse>((resolve, reject) => {
      this.client.login(request, {}, (err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    });
  }
}
