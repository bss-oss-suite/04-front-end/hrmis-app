import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(private authService: AuthService) {}

  login(username: string, password: string): void {
    this.authService
      .login(username, password)
      .then((response) => {
        const token = response.getToken();
        // Handle successful login, e.g., store token in local storage
        console.log('Login successful. Token:', token);
      })
      .catch((error) => {
        // Handle login error
        console.error('Login error:', error);
      });
  }
}
