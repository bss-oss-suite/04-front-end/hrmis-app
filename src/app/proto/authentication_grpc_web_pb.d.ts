import * as grpcWeb from 'grpc-web';

import * as authentication_pb from './authentication_pb';

export class AuthServiceClient {
  constructor(
    hostname: string,
    credentials?: null | { [index: string]: string },
    options?: null | { [index: string]: any }
  );

  login(
    request: authentication_pb.LoginRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (
      err: grpcWeb.RpcError,
      response: authentication_pb.LoginResponse
    ) => void
  ): grpcWeb.ClientReadableStream<authentication_pb.LoginResponse>;
}

export class AuthServicePromiseClient {
  constructor(
    hostname: string,
    credentials?: null | { [index: string]: string },
    options?: null | { [index: string]: any }
  );

  login(
    request: authentication_pb.LoginRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<authentication_pb.LoginResponse>;
}
